<?php

namespace Database\Seeders;

use App\Models\Location;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use MatanYadaev\EloquentSpatial\Objects\Point;

class MapCSVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Location::factory(10)->create();
        if (($fp = fopen(__DIR__ . "/map_data.csv", "r")) !== FALSE) {

            while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
                echo "Location: $data[0]: " . $data[1] . ", " . $data[2] . "\n";
                Location::factory()->create([
                    'name' => $data[0],
                    'coords' => new Point($data[1], $data[2]),
                ]);
            }
            fclose($fp);
        }
    }
}
