<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/', function () {
    $url = $_REQUEST['lat'] ?? '';
    $url .= (isset($_REQUEST['long'])) ? ',' . $_REQUEST['long'] : '';
    $url .= (isset($_REQUEST['radius'])) ? ':' . $_REQUEST['radius'] : '';
    $url .= $_REQUEST['unit'] ?? '';
    return redirect('/api/locations/radius/'.$url);
});

Route::redirect('/api', '/');

Route::controller(\App\Http\Controllers\LocationController::class)->group(function (){
    Route::get('/api/locations/radius/{lat},{long}:{radius?}{unit?}', 'radiusSearch')->name('locations')->where(['lat', 'long'], '[0-9\.\-]+')
        ->whereNumber('radius')->whereIn('unit', ['m', 'km', 'mi']);
});
