<p style="text-align: center;"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p style="text-align: center;">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About This Project

This is a simple implementation of a REST API with a single entry point.
The app expects a GET request on /api/locations/radius and will perform a database search, finding any locations with lat/long coordinates within a specific radius. It will them show all locations found (if any) along with the calculated spherical distance from the starting point provided.

At this first moment it's being decided to render the results in a web page for readability and verification, but the API should only return a JSON.

## License

This app, as the Laravel framework, is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
