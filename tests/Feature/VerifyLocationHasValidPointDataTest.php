<?php

namespace Tests\Feature;

use App\Models\Location;
use MatanYadaev\EloquentSpatial\Objects\Point;
use Tests\TestCase;

class VerifyLocationHasValidPointDataTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_location_has_valid_spatial_data()
    {
       $data = Location::inRandomOrder()->first();
       $coords = $data->coords;
       //echo "Lat: " . $coords->latitude;
      // echo "Long: " . $coords->longitude;
       $this->assertTrue($coords instanceof Point);
    }
}
