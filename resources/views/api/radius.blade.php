<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials.head')

    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <h1 class="text-gray-700">{{ $locationsCount }} Location{{ $locationsCount > 1 ? 's' : '' }} Found</h1>
                <h4 class="text-gray-600">within {{ $radius }}{{ $unit }} of {{ empty($name) ? '('.$lat.','.$long.')' : $name }}</h4>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg p-6">

                    @foreach($locationsWithinDistance as $location)
                        <div class="grid grid-cols-1 md:grid-cols-1">
                            @if($location->name == $name)
                                <h5 class="text-gray-500 mb-3">{{ $name }} <span class="text-small">(less than 1m)</span></h5>
                            @else
                                @php
                                    /* Convert the distance from meters to km or miles*/
                                        $distance = match ($unit) {
                                            'km' => round($location->distance / 1000, 1),
                                            'mi' => round($location->distance / 1609.34, 1),
                                            default => round($location->distance, 0),
                                        };
                                @endphp
                                <h5 class="text-gray-900">{{ $location->name }} <span class="text-small">({{ $distance }}{{ $unit }} away)</span></h5>
                            @endif
                        </div>
                    @endforeach

                </div>
                <div class="mt-8 h-16"></div>
                @include('partials.footer')

            </div>
        </div>
    </body>
</html>
