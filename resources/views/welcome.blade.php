<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials.head')
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                @include('partials.laravel-logo')

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-1">
                        <div class="p-6">
                            <div class="flex items-center">
                                <i class="fa-solid fa-2xl fa-barcode text-gray-500"></i>
                                <div class="ml-4 text-lg leading-7 font-semibold"><p class="text-gray-900 dark:text-white my-0">Project</p></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-3 text-gray-600 dark:text-gray-400 text-sm">
                                    <p>This is proof of concept for a <b>Map API</b> using Laravel. We're using the "Laravel Eloquent Spatial" package by <a href="https://github.com/MatanYadaev/laravel-eloquent-spatial">Matan Yadaev</a> that facilitates the use of Spatial Data format in MySQL.
                                        Built with Homestead and Vagrant, using Laravel 9, PHP 8.1 and MySQL 8.</p>

                                    <p>The real API should only return a JSON encoded version of <i>$locationsWithinDistance</i> but in order to improve readability, during the POC phase we're using a simple HTML output to list the map entries and the feature that retrieves known point names based on an approximated point.
                                    <br>Once the initial verification is done we will simply return an API route with the json encoded result rather than the web route.</p>

                                    <p>Made by Leni Neto.</p>
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                            <div class="flex items-center">
                                <i class="fa-solid fa-2xl fa-code text-gray-500"></i>
                                <div class="ml-4 text-lg leading-7 font-semibold"><p class="text-gray-900 dark:text-white my-0">API</p></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-3 text-gray-600 dark:text-gray-400 text-sm">
                                    This is the app's main feature. The API currently only have one GET method:
                                    <div class="p-2 mt-2 mb-2 text bg-gray-100">
                                        <pre class="ml-4 text-gray-700 mb-0 p-1">/api/radius/<i>lat</i>,<i>long</i>:<i>radius</i><i>[unit]</i></pre>
                                    </div>
                                    <p>
                                        <b><i>lat:</i></b> latitude (negative values for south)<br>
                                        <b><i>long:</i></b> longitude (negative values for west)<br>
                                        <b><i>radius:</i></b> search radius (in meters, unless a unit is set)<br>
                                        <b><i>unit:</i></b> optional parameter, can be 'm' for meters, 'km' for kilometers or 'mi' for miles
                                    </p>
                                    <p>
                                        <b>You can use the form below as a convenience to test the API</b>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                            <div class="flex items-center">
                                <i class="fa-solid fa-2xl fa-gears text-gray-500"></i>
                                <div class="ml-4 text-lg leading-7 font-semibold"><p class="text-gray-900 dark:text-white my-0">Features</p></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-3 text-gray-600 dark:text-gray-400 text-sm">
                                    <ul>
                                        <li>Location model and controller</li>
                                        <li>DB factory capable of creating random locations using city names and valid lat/long coordinates via faker</li>
                                        <li>DB seeder that imports the CSV file provided</li>
                                        <li>Laravel Eloquent Spatial performing spherical distance calculations from DB records</li>
                                        <li>Detects if the starting point is within 1m of an existing location and refer to it by name</li>
                                        <li>Allows radius distance in meters, kilometers and miles, performing conversions on-the-fly</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-16 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-1">
                        <div class="p-6">
                            <form name="add-blog-post-form" id="add-blog-post-form" method="post" action="">
                                @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa-solid fa-location-dot"></i></span>
                                            </div>
                                            <input type="text" id="lat" name="lat" class="form-control" placeholder="Latitude" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa-solid fa-location-dot"></i></span>
                                            </div>
                                            <input type="text" id="long" name="long" class="form-control" placeholder="Longitude" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row mt-4">
                                    <div class="col-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa-solid fa-location-crosshairs"></i></span>
                                            </div>
                                            <input type="text" id="radius" name="radius" class="form-control" placeholder="Radius" required>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <select class="form-control" id="unit" name="unit">
                                                <option value="m">meters</option>
                                                <option value="km">kilometers</option>
                                                <option value="mi">miles</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-info btn-block">Find Locations</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @include('partials.footer')
            </div>
        </div>
    </body>
</html>
