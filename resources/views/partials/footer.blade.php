<div class="flex justify-center mt-4 sm:items-center sm:justify-between">
    <div class="text-center text-sm text-gray-500 sm:text-left">
        <div class="flex items-center">

            @if (Route::currentRouteName() == 'locations')
                <i class="fa-solid fa-lg fa-house text-gray-400 w-5"></i>

                <a href="/" class="ml-1 mr-2 underline">
                    Back
                </a>
            @endif

            <i class="fa-regular fa-lg fa-heart text-gray-400 w-5"></i>

            <a href="https://lenineto.com/" class="ml-1 mr-2 underline" target="_blank">
                Leni Neto
            </a>

            <i class="fa-solid fa-lg fa-code-branch text-gray-400 ml-4 w-5"></i>

            <a href="https://gitlab.com/lenineto/map-api/" class="ml-1 underline" target="_blank">
                Gitlab
            </a>

        </div>
    </div>

    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
        Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
    </div>
</div>
