<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials.head')

    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <h1 class="text-gray-900">An error has occurred</h1>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-1 p-6">
                        <div class="mt-8"></div>
                        {{ $error }}
                        <div class="mt-8"></div>

                    </div>
                </div>
                <div class="mt-8 h-16"></div>
                @include('partials.footer')

            </div>
        </div>
    </body>
</html>
