<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\SpatialBuilder;

class Location extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @method static SpatialBuilder query()
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'coords',
    ];

    protected $casts = [
        'coords' => Point::class,
    ];

    /**
     * Disable timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * New Eloquent query for spatial data
     * @param $query
     * @return SpatialBuilder
     */
    public function newEloquentBuilder($query): SpatialBuilder
    {
        return new SpatialBuilder($query);
    }

}
