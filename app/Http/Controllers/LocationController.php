<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use MatanYadaev\EloquentSpatial\Objects\Point;

class LocationController extends Controller
{

    /**
     * Finds all locations within the radius around the center point
     *
     * @param float $lat
     * @param float $long
     * @param int $radius
     * @param string $unit
     * @return Application|Factory|View
     */
    public function radiusSearch(float $lat, float $long, int $radius = 3000, string $unit = 'm'): View|Factory|Application
    {
        /* Convert the radius from km or miles to meters */
        $radius_m = match ($unit) {
            'km' => $radius * 1000,
            'mi' => $radius * 1609.34,
            default => $radius,
        };

        $center = new Point($lat, $long);
        $locationsWithinDistance = Location::query()->whereDistanceSphere('coords', $center, '<', $radius_m)->orderByDistanceSphere('coords', $center)->withDistanceSphere('coords', $center)->get();
        if (is_countable($locationsWithinDistance)) {
            if (count($locationsWithinDistance) > 0) {
                $locationsCount = count($locationsWithinDistance);
                /* If the closest location is within 1m of our center point, we'll use it's name for our center point */
                $closestLocation = $locationsWithinDistance->first();
                ($closestLocation->distance < 1 ) ? $name = $closestLocation->name : $name = '';
                return view('api.radius', compact('name','lat', 'long', 'radius', 'unit', 'locationsCount', 'locationsWithinDistance'));
            }
            else {
                $error = "Could not find any locations. Please verify your coordinates or try a bigger radius.";
            }

        } else {
            $error = "Missing or invalid parameter. Required parameters are: lat, long and radius";
        }
        return view('error', compact('error'));
    }

}
